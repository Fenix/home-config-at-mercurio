;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
             (gnu home services shells))

(home-environment
  ;; Below is the list of packages that will show up in your
  ;; Home profile, under ~/.guix-home/profile.
  (packages (specifications->packages (list "xfce4-power-manager"
                                            "python"
					    "rofi"
					    "dino"
					    "xarchiver"
                                            "yt-dlp"
                                            "libinput"
                                            "i3lock"
                                            "ffmpeg"
                                            "zsh-syntax-highlighting"
                                            "zsh-autosuggestions"
                                            "zsh-autopair"
                                            "zsh"
                                            "xfce4-screenshooter"
                                            "qutebrowser"
                                            "gitg"
                                            "artanis"
                                            "abiword"
                                            "rofi"
                                            "ungoogled-chromium"
                                            "network-manager-openvpn"
                                            "calibre"
                                            "vlc"
                                            "audacity"
                                            "gvfs"
                                            "obs"
                                            "thunar"
                                            "mpv"
                                            "icecat"
                                            "moc"
                                            "xfce4-settings"
                                            "xf86-input-wacom"
                                            "redshift"
                                            "ristretto"
                                            "speedtest-cli"
                                            "gimp"
                                            "xarchiver"
                                            "glib"
                                            "docker"
                                            "thunar-volman"
					    "rofi"
                                            "nyxt"
					    "node"
                                            "arandr"
                                            "openssh"
                                            "bluez-alsa"
                                            "bluez"
                                            "blueman"
                                            "jekyll"
                                            "dino"
                                            "emacs"
                                            "ruby"
                                            "pavucontrol"
                                            "pulseaudio"
                                            "git"
                                            "alacritty"
                                            "recutils"
                                            "openvpn"
                                            "rsync"
                                            "glibc"
                                            "mutt"
                                            "fuse"
                                            "go"
                                            "fontconfig"
                                            "qemacs"
                                            "unzip"
                                            "curl"
                                            "ncdu"
                                            "haunt"
                                            "iptables"
                                            "binutils"
                                            "gcc"
                                            "abook"
                                            "make"
                                            "tar"
                                            "xrandr"
                                            "zip"
                                            "syncthing"
					    "syncthing-gtk"											
                                            "alsa-utils"
                                            "tidy"
                                            "htop"
                                            "tmux")))

  ;; Below is the list of Home services.  To search for available
  ;; services, run 'guix home search KEYWORD' in a terminal.
  (services
   (list (service home-bash-service-type
                  (home-bash-configuration
                   (aliases '((".." . "cd ..") ("..." . "cd ../../../")
                              ("...." . "cd ../../../../")
                              ("....." . "cd ../../../../")
                              (".4" . "cd ../../../../")
                              (".5" . "cd ../../../../..")
                              ("yotuve" . "echo youtube-dl --ignore-errors -f bestaudio --extract-audio --audio-format vorbis https://... ")))
                   (bashrc (list (local-file
                                  "/home/fenix/my-home-config/.bashrc"
                                  "bashrc")))
                   (bash-profile (list (local-file
                                        "/home/fenix/my-home-config/.bash_profile"
                                        "bash_profile"))))))))
