

# https://www.freecodecamp.org/news/bashrc-customization-guide/

# Video

alias 19-Stack='xrandr && xrandr --output LVDS-1 --auto --pos 0x1024 --rotate normal --output VGA-1 --primary --auto' 

alias 19-2Stack='xrandr && xrandr --output LVDS-1 --primary --mode 1366x768 --pos 0x1024 --rotate normal --output VGA-1 --mode 1280x1024 --pos 0x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output HDMI-3 --off --output DP-2 --off --output DP-3 --off'

alias 19-LG='xrandr && xrandr --output LVDS-1 --auto --output VGA-1 --mode 1360x768'


alias 19-HP='xrandr && xrandr --output LVDS-1 --off --output VGA-1  --mode 1280x1024 --pos 0x0 --rotate normal --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output HDMI-3 --off --output DP-2 --off --output DP-3 --off'

alias 15='xrandr && xrandr --output LVDS-1 --primary --mode 1366x768 --pos 0x1024 --rotate normal --output VGA-1 --off  --output HDMI-1 --off --output DP-1 --off --output HDMI-2 --off --output HDMI-3 --off --output DP-2 --off --output DP-3 --off'

# i3wm
# https://faq.i3wm.org/question/591/move-workspace-to-other-monitor.1.html
alias i3wmup='i3-msg move workspace to output up'
alias i3wmdown='i3-msg move workspace to output down'

# ahorra energía
alias wifi1='sudo iwconfig wlp3s0 txpower 1mw'
alias wifi3='sudo iwconfig wlp3s0 txpower 3mw'
alias wifi6='sudo iwconfig wlp3s0 txpower 6mw'
alias wifi11='sudo iwconfig wlp3s0 txpower 11mw'
alias wifi15='sudo iwconfig wlp3s0 txpower 15mw'
alias wifi22='sudo iwconfig wlp3s0 txpower 22mw'
alias wifi30='sudo iwconfig wlp3s0 txpower 30mw'
alias wifi-off='sudo iwconfig wlp3s0 txpower off'

# cuida tus ojos -redshift - gtk
# Latitude Madrid : 40.5137:-3.8671
# How do I setup a configuration file?

# A configuration file is not required but is useful for saving custom configurations and manually defining the location in case of issues with the automatic location provider. An example configuration can be found in redshift.conf.sample.

# The configuration file should be saved in the following location depending on the platform:

#     Linux/macOS: ~/.config/redshift/redshift.conf



alias red='redshift -l 40.5137:-3.8671  -t 6500:4500'


alias em='emacs -nw'

alias firefoxdev='/opt/firefox-dev/firefox'

alias sba='source ~/.bash_aliases'

alias init='sudo iwconfig wlp3s0 txpower 1mw && redshift -l 40.5137:-3.8671  -t 6500:4500'

alias al10m='sleep 10m && vlc ~/tibetan-gong.ogg'
alias al15m='sleep 15m && vlc ~/tibetan-gong.ogg'
alias al20m='sleep 20m && vlc ~/tibetan-gong.ogg'
alias al25m='sleep 25m && vlc ~/tibetan-gong.ogg'
alias al30m='sleep 30m && vlc ~/tibetan-gong.ogg'


# Move to the parent folder.
alias ..='cd ..;pwd'

# Move up two parent folders.
alias ...='cd ../..;pwd'

# Move up three parent folders.
alias ....='cd ../../..;pwd'

# Press c to clear the terminal screen.
alias c='clear'

# Press h to view the bash history.
alias h='history'

# Display the directory structure better.
alias tree='tree --dirsfirst -F'

# Make a directory and all parent directories with verbosity.
alias mkdir='mkdir -p -v'

# View the calender by typing the first three letters of the month.

alias jan='cal -m 01'
alias feb='cal -m 02'
alias mar='cal -m 03'
alias apr='cal -m 04'
alias may='cal -m 05'
alias jun='cal -m 06'
alias jul='cal -m 07'
alias aug='cal -m 08'
alias sep='cal -m 09'
alias oct='cal -m 10'
alias nov='cal -m 11'
alias dec='cal -m 12'






alias ping-cisco='ping www.cisco.com'
alias ping-router='ping 192.168.1.1'
alias speed='cd ~/Utils && ./speedtest-cli'
alias Utils='cd ~/Utils'
alias Usb='cd /media/jordi/USB32Gb/'

#
# mis atajos, creados a partir de enlaces simbolicos
#

# alias Notas='cd ~/.aa_eNotes'
# alias Personal='cd ~/.aa_Droid-Personal'
# alias Librebits='cd ~/.aa_Librebits'


alias html='cd /srv/www/htdocs/'
alias uhtml='cd /run/media/jordi/USB32Gb/uhtml'
alias Html='cd /run/media/jordi/USB32Gb/Html'
alias Nodejs='cd ~/DATA/Html/nodejs'
alias temp='cd ~/DATA/temp'

alias Desk='cd ~/Desktop'
alias Data='cd ~/DATA'
alias Tmp='cd ~/Tmp'
alias Video='cd ~/DATA/VIDEO'

alias Ip='em ~/Documents/Ip.txt'

alias Hang='cd ~/Music/Hang-drum'
alias Enotes='em ~/DATA/sincroniza/eNotes'
alias Slibrebits='em ~/DATA/sincroniza/Librebits'
alias javascript='cd ~/DATA/Gitlab/JavaScript_Essentials/Chap06'
alias Sinc='cd ~/DATA/sincroniza'

alias ArrobaCloud='cd /Nextcloud/ArrobaCloud/'


alias Jekyll='cd ~/Jekyll'
alias mls='cd ~/Jekyll/mls.com'
alias tco='cd ~/Jekyll/tco.com'
alias latjcc='cd ~/Jekyll/latjcc'

alias Org='cd ~/Org'

alias Usecue='cd ~/Usecue'
alias Usego='cd ~/Usecue/Hugo'
alias Usekyll='cd ~/Usecue/Jekyll'

alias Hugo='cd ~/Hugo'
alias BB='cd ~/Jekyll/basically-basic'
alias Latj='cd ~/Jekyll/latjcc'
alias RoR='cd ~/RubyOnRails'
alias Cajon3s='cd ~/M@S/Cajon3s'
alias M@S='cd ~/M@S/'
alias Librebits='cd ~/Librebits/'
alias Utils='cd ~/Utils'
alias Blog='cd ~/Blog'
alias Pictures='cd ~/Pictures'

# Dropbox
# https://wiki.debian.org/dropbox
alias dropbox='~/.dropbox-dist/dropboxd'

alias Dbox='cd ~/Dropbox'


alias S2V='cd ~/Terramondo-Librebits-projects/soul2visionV2'
alias Terramondo='cd ~/Dropbox/Terramondo-Librebits-projects'
alias Musica='cd ~/Music'
alias Instrumental='cd ~/DATA/INSTRUMENTAL'
alias Yoga='cd ~/Music/YogaMusic'
alias Gitlab='cd /media/jordi/USB32Gb/Gitlab'
alias Git='cd /media/jordi/USB32Gb/Git'


alias mdn='cd ~/mdn_content/ && sudo yarn start'



alias c='clear'

alias sba='source ~/.bash_aliases'


## youtube-dl
# youtube-dl --ignore-errors -f bestaudio --extract-audio --audio-format mp3
# youtube-dl --ignore-errors -f bestaudio --extract-audio --audio-format vorbis https://.. 

alias yotuve='echo youtube-dl --ignore-errors -f bestaudio --extract-audio --audio-format vorbis https://... '

# Httrack
# httrack --mirror --robots=0 --stay-on-same-domain --keep-links=0 --path example.net --max-rate=409600 --connection-per-second=4 --sockets=8 --quiet https://example.net/ -* +example.org/*
# OK $ httrack --mirror --robots=0 --stay-on-same-domain --keep-links=0 --path example.org --quiet https://example.org/ -* +example.org/*

# nmap

# nmap --open -p T:22 10.1.5.0/24 # Find the hosts on the 10.1.5.0/24 that have possibly open SSH ports on the default TCP port 22. Great for finding that headless orange pi you just started.


# listado

alias lsa='ls --color -l -A -hF --group-directories-first | more'
alias lsa='ls --color -l -A -hF | more'
alias lat='ls -lhAt'
alias la='ls --color -l -A -hF | more'

alias faqui='find . -name'

# Renombra  *.html >> *.php
# rename -n 's/\.php$/\.html/' *.php

# sobre paquetes instalados
# p.ej : PHP7
# dpkg --list | grep php | awk '/^ii/{ print $2}'


# Grep < regexp
# https://linuxconfig.org/match-beginning-and-end-of-the-filename-using-meta-characters-and-regex
## to display all files and directories starting with "a" and ending with "k" ( lowercase ! ) character.
## $ ls | grep ^a.*k$

# Rename - bulk
## https://ostechnix.com/how-to-rename-multiple-files-at-once-in-linux/

##
# MANTENIMIENTO dEBIAN GNU Linux
# paquetes deb huerfanos
# sudo deborphan --guess-all | xargs sudo aptitude purge -y
alias upgrade='sudo apt update && sudo apt upgrade'

## à la Fanta ?
# alias bashlsd='man $(basename $(find /usr/share/man/ -type f |sort -R|head -n1) | sed 's/\.[^.]*\.bz2$//' | cut -d "." -f 1)'


# MUMBLE
# client

# config. shortcut - push to Talk 

## Descargar 'clon' Website local
# e.g. https://news.gandi.net/
# wget --wait 1 -x -mk https://news.gandi.net/
# wget -m --wait=9 --limit-rate=10K https://news.gandi.net/


#############
# JavaScript  - VueJS
# NPM - Yarn
#
## 1. vue-cli installation
# First, we global install vue-cli like so:
# > npm install -g vue-cli

# Depending on your npm setup, you might need sudo on above command.
#  \!/ sudo necesario ^^


# APACHE Server
# logging : /var/log/apache2/error.log
alias apachelog='sudo tail -100 /var/log/apache2/error.log'
alias apacheaccess='sudo tail -100 /var/log/apache2/access.log'

alias a2stop='sudo systemctl stop apache2'
alias a2start='sudo systemctl start apache2'
alias a2restart='sudo systemctl restart apache2'
alias a2reload='sudo systemctl reload apache2'
alias a2status='sudo systemctl status apache2'

## DRUPAL
# Drupal internal server | no apache2 config needed
# alias druserver='vendor/bin/drush runserver'
alias drs='vendor/bin/drush run server'

alias chuw='chmod u+w sites/default/'

# listar procesos en orden decreciente	por consumo de memoria RAM

alias psmem='ps -eo pmem,pcpu,rss,vsize,args | sort -k 1 -r | less'


# listar ficheros/directorios unicamente
alias lf="ls -l | egrep -v '^d'"
alias ldir="ls -l | egrep '^d'"
alias lap='la  --color=always | less -R'

# zypper Package manager | openSUSE
# roll-release
alias sz='sudo zypper'
alias szd='sudo zypper dup'


## MANTENIMIENTO debian GNU Linux
#

# à la http://www.waveguide.se/?article=how-to-quickly-remove-all-unused-packages-under-debian
# Bash Script ?
# sudo deborphan --guess-all | xargs sudo aptitude purge -y

## Docker
#

alias dockly='docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock lirantal/dockly'

# systemd & docker
#sudo systemctl start docker.service 
#sudo systemctl status docker.service 

# alias dockeron='sudo systemctl start docker.service'
# alias dockeroff='sudo systemctl stop docker.service'
# alias dockerstat='sudo systemctl status docker.service'

alias dockerstart='sudo systemctl start docker.service && sudo systemctl start docker.socket'
alias dockerstop='sudo systemctl stop docker.service && sudo systemctl stop docker.socket'
alias dockerstatus='sudo systemctl status docker.service && sudo systemctl status docker.socket'

# Telega - Telegram@Emacs
# requiere Dcoker para la libreria TDlib si esta no ha sido compilada por debian con éxito?

## Ruby
#


# Hugo Golang

# alias hugodocs='cd ~/hugoDocs && hugo serve -p 1111'
alias hugocodex='cd ~/hugocodex && hugo serve -p 3333'
alias nock='cd ~/nock_hugo && hugo serve -p 1212'

# publicar Jekyll  basically basic
alias lbspub="cd ~/Hugo/librebits.dev && hugo && rsync -avz  public/ sirio:/var/www/html/public"

alias lbs='cd ~/Hugo/librebits.dev/'

## Jekyll to Hugo PMTAMSTELVEEN .nl  porting
alias pmtamstelveen='cd ~/pmtamstelveen'
alias pmt_hugo='cd ~/pmt_hugo/'

# publicar Jekyll  basically basic
alias rbb='rsync -avz ~/Jekyll/basically-basic/_site/ sirio:/var/www/html/blog'
alias bb='cd ~/Jekyll/basically-basic/'

## PHP
#

alias Laravel='cd ~/DATA/Html/Laravel'
alias LaraCourse='cd ~/DATA/Html/Laravel/Projects\ in\ Laravel\:\ Learn\ Laravel\ Building\ Projects\ Course\ Intro/'

# Projects in Laravel: Learn Laravel Building Projects Course Intro

## Python 
#

alias py='python3'
alias Python='cd ~/Python'
alias mailsnip='cd ~/DATA/Python/Automating/Chap06'

# [ ] agregar al 'PATH' ?!


# Activating a virtualenv

# source env/bin/activate


# ======================================================================
# scw - Scaleway CLI 
# ======================================================================

alias scwlogin='cd ~/golang && scw login && scw ps'
alias scwa='scw ps -a'


# Since I use Git a lot, that would be a great command to create an alias for.

# # View Git status.
# alias gs='git status'

# # Add a file to Git.
# alias ga='git add'

# # Add all files to Git.
# alias gaa='git add --all'

# # Commit changes to the code.
# alias gc='git commit'

# # View the Git log.
# alias gl='git log --oneline'

# # Create a new Git branch and move to the new branch at the same time. 
# alias gb='git checkout -b'

# # View the difference.
# alias gd='git diff'


# Reset
alias grh='git reset --hard HEAD~1'





## Git workflow

# $ git pull
# Pulling without specifying how to reconcile divergent branches is
# discouraged. You can squelch this message by running one of the following
# commands sometime before your next pull:
# 
#   git config pull.rebase false  # merge (the default strategy)
#   git config pull.rebase true   # rebase
#   git config pull.ff only       # fast-forward only
# 
# You can replace "git config" with "git config --global" to set a default
# preference for all repositories. You can also pass --rebase, --no-rebase,
# or --ff-only on the command line to override the configured default per
# invocation.




## Git
#

alias g='git'
alias ga='git add'
alias gcm='git commit -m'
alias gcb='git checkout -b'

alias gacm='git add . && git commit -m'


# git push a-la https://matthiasloibl.com/posts/git-push-all-branches/
# git push --all origin . \!/
# git push --all ?


# git pull --all | sycn todas las ramas desde el remoto a local
# a-la milkovsky https://stackoverflow.com/questions/4318161/can-git-pull-all-update-all-my-local-branches

# for remote in `git branch -r`; do git branch --track $remote; done
# git fetch --all
# git pull --all



# git pull all branches <- ex git-up ; obsolete
# https://github.com/aanand/git-up
# git config --global alias.up 'pull --rebase --autostash'// ver ~/Org/git-flow-art.org section




# git repo size ?
# git gc
# git count-objects -vH
# o https://github.com/github/git-sizer/#getting-started
# git-sizer -v
# https://paulhammant.com/2016/08/15/introducing-git-size-command-and-visualization/

# decorar git branches
alias gdecob='git log --all --graph --decorate --oneline --simplify-by-decoration'

# mostrar árbol 
alias garbol='git log --graph --simplify-by-decoration --pretty=format:'%d' --all'

alias gramal='git show-branch --list'

alias gs='git status'
alias gadb='git add storage/drupal.sqlite'
alias gacj='git add composer.json composer.lock'
alias gaf='git add web/sites/default/files/'
alias glo='git log --oneline'



alias gaa='git add .'

# git stash
alias gss='git stash save'
alias gsd='git stash drop'


# Discard changes - For all unstaged files in current working directory use:
alias gfo='git checkout -- .'





# Drush alias

alias dcr='drush cache-rebuild'
alias drs='drush runserver'
## drush config-set system.theme default THEME  // as default Theme
alias dct='drush config-set system.theme default'


## Actividad en la red
# netstat -lntp

#######
# CLI
# ej: buscar paquetes entorno a firewall
#
# apt-cache search firewall | grep firewall | less
#
#



#######
# LXCs

alias debian8='sudo lxc-start -n debian8 -d'
alias milxc='sudo lxc-start -n "$lxc" -d'


# Getting started a la Stephane Graber
# https://linuxcontainers.org/lxc/getting-started/
# https://discuss.linuxcontainers.org/
# Container templates ?
# $ sudo apt install lxc-templates
# $ lxc checkconfig(OAA?)
# $ sudo lxc-create -t download -n privileged-container
# Will create a new "privileged-container" privileged container on your system using an image from the download template.
# ... crear un nuevo "unprivileged-container" ya es otro cantar ... ? (requiere systemd-user )

# Gitlab wiki - start Gollum server
#
# gollum
# == Sinatra/1.3.5 has taken the stage on 4567 for development with backup from Thin
# >> Thin web server (v1.5.0 codename Knife)
# >> Listening on 0.0.0.0:4567, CTRL+C to stop
#
# Hay que estar en ../repositorio.wiki  \!/
##
alias wiki='gollum'

alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../..'

# suspend \!/

# alias suspend15='echo 'sudo pm-suspend' | at now + 15 minutes'
# alias suspend30='echo 'sudo pm-suspend' | at now + 30 minutes'
# alias suspend60='echo 'sudo pm-suspend' | at now + 60 minutes'
# alias suspend90='echo 'sudo pm-suspend' | at now + 90 minutes'
# alias suspend120='echo 'sudo pm-suspend' | at now + 120 minutes'

# shutdown
alias shutdown90='sudo shutdown -P 90'
alias shutdown60='sudo shutdown -P 60'
alias shutdown30='sudo shutdown -P 30'


# hibernate
alias hiber='sudo hibernate'
alias hiber30='echo 'hibernate' | sudo at now + 30 min'
alias hiber120='echo 'hibernate' | sudo at now + 120 min'
alias hiber60='echo 'hibernate' | sudo at now + 60 min'


# IRC freenode
# https://freenode.net/kb/answer/registration
# SASL auth https://freenode.net/kb/answer/pidgin

# Jekyll
alias jekyllsite='bundle exec rake site:preview'

alias jek="bundle exec jekyll"
alias jekb="bundle exec jekyll build"
alias jekc="bundle exec jekyll clean"
alias jeks1="bundle exec jekyll s -P 4001"
alias jeks2="bundle exec jekyll s -P 4002"
alias jeks3="bundle exec jekyll s -P 4003"
alias jeks6="bundle exec jekyll s -P 4006"
alias jeks9="bundle exec jekyll s -P 4009"
alias jeks10="bundle exec jekyll s -P 4010"
alias jeks11="bundle exec jekyll s -P 4011"
alias jeks12="bundle exec jekyll s -P 4012"
alias jeks22="bundle exec jekyll s -P 4022"
alias jeks33="bundle exec jekyll s -P 4033"
alias jekdocs="cd ~/Jekyll/jekyll/docs && bundle exec jekyll s"

alias jekmake="cp ~/Makefile/Makefile-Jekyll Makefile"
alias jekgit="cp ~/Makefile/gitignore/gitignore-jekyll .gitignore"

#         .bundle/
#        Makefile
#        vendor/




# Hugo | Golang
alias hugs='hugo -D server'


# gestion X , vga ...
alias cloned='xrandr --output VGA1 --auto && xrandr --output LVDS1 --auto'
alias dual='xrandr --output VGA1 --auto --left-of LVDS1'
#  ?? ^^
alias laptop='xrandr --output LVDS1 --auto && xrandr --output VGA1 --off'
alias laptop-off='xrandr --output LVDS1 --off'
alias vga='xrandr --output VGA1 --auto && xrandr --output LVDS1 --off'
alias vga-off='xrandr --output VGA1 --off'

# MDN content
alias mdn='cd ~/mdn_content && yarn start'

# youtube-dl
#            | playlists
#
# https://www.ostechnix.com/youtube-dl-tutorial-with-examples-for-beginners/
#
# youtube-dl -i -f mp4 --yes-playlist "https://www.youtube.com/watch?v ..." 
#
# youtube-dl -i playlist-ID  ( list=PLpVC00PAQQxG0sW9YOueVgouRp4aj1bng ... e.g.)
#
# youtube-dl -cit https://www.youtube.com/playlist?list=PLttJ4RON7sleuL8wDpxbKHbSJ7BH4vvCk
#            | playlist extract audio

#  By default, Youtube-dl will save the audio in Ogg (opus) format.

# If you prefer to download any other formats, for example mp3, run:

# $ youtube-dl -x --audio-format mp3 https://www.youtube.com/watch?v=7E-cwdnsiow

# This command will download the audio from the given video/playlist, convert it to an MP3 and save it in the current directory. Please note that you should install either
# ffmpeg or avconv to convert the file to mp3 format.

# OK !
# youtube-dl --extract-audio --audio-format mp3 --output "%(uploader)s%(title)s.%(ext)s" http://www.youtube.com/watch?v=rtOvBOTyX00
#
# https://askubuntu.com/questions/630134/how-to-specify-a-filename-while-extracting-audio-using-youtube-dl
#########

# incremental Backup

alias backup='rsync -aAXv --delete --exclude={"/home/*/.thumbnails/*","/home/*/.cache/mozilla/*","/home/*/.cache/chromium/*","/home/*/.local/share/Trash/*","/home/*/.gvfs"} ~/ /media/jordi/PETACA/BACKUP/jordi/'

# RSync
# rsync -avr --progress --links  192.168.0.20:/var/www/html /var/www/html

# # rsync -avr --progress --links  /mnt/sirio/terramondo8.9.0 /run/media/jordi/USB32Gb/uhtml/terramondo8.9.0

# Services

## LAMP - Apache - SQL server
alias lampon='sudo systemctl start apache2 && sudo systemctl start mariadb.service'
alias lampoff='sudo systemctl start apache2 && sudo systemctl start mariadb.service'
alias a2start='sudo systemctl start apache2'
alias a2stop='sudo systemctl stop apache2'
alias a2restart='sudo systemctl restart apache2'

## LAMP - Apache - SQL server
alias lampon='sudo systemctl start apache2 && sudo systemctl start mariadb.service'
alias lampoff='sudo systemctl start apache2 && sudo systemctl start mariadb.service'
alias a2start='sudo systemctl start apache2'
alias a2stop='sudo systemctl stop apache2'
alias a2restart='sudo systemctl restart apache2'

# SSH - Git
# https://docs.gitlab.com/ee/ssh/
# check : ssh -T git@gitlab.com ? as jordila
## check : ssh -T git@gitlab.com -flow ? as jlaF0w

# HTTRACK - web scrapping
# httrack https://app.usecue.com/ -O usecueApp -N "%h%p/%n/index%[page].%t" -WqQ%v --robots=0 --footer ''
#                   URL              localFolder

##
# SSHFS
#
# https://www.digitalocean.com/community/tutorials/how-to-use-sshfs-to-mount-remote-file-systems-over-ssh

# sudo mkdir /mnt/sshfs <--replace "droplet" whatever you prefer
# sudo sshfs -o allow_other,default_permissions,IdentityFile=~/.ssh/id_rsa root@xxx.xxx.xxx.xxx:/ /mnt/sshfs
# sshfs Sirio server

alias sshfssoa2='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect sirio:/etc/apache2  /mnt/sirio-vhosts'
alias sshfssoh='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect sirio:/var/www/html  /mnt/sirio-html'
alias sshfssoj='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect sirio:/home/jordi/  /mnt/sirio@jordi/'
alias sshfssoe='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect sirio:/etc/  /mnt/sirio@etc/'
alias sshfssob='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect,idmap=user bot@sirio:/home/bot/slixmpp  /mnt/sirio@bot/'



# sshfs Proxima Centauri server
alias sshfspaa2='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect proximacentauri:/etc/apache2  /mnt/proximacentauri-vhosts'
alias sshfspah='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect proximacentauri:/var/www/html  /mnt/proximacentauri-html'
# alias sshfspae='sudo sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect proximacentauri:/etc  /mnt/proximacentauri-etc'

alias sshfspae='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect proximacentauri:/etc  /mnt/proximacentauri@etc'
# alias sshfspaj='sshfs -o default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect proximacentauri:/home/jordi/  /mnt/proximacentauri@jordi/'


# sshfs -o allow_other,default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect asterix:/home/admin/ /mnt/asterix

alias sshfsaxh='sshfs -o  default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect admin@asterix:/var/www/html /mnt/asterix@html -p 22341'
alias sshfsaxa='sshfs -o  default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect admin@asterix:/etc/apache2 /mnt/asterix@apache2 -p 22341'
alias sshfsaxj='sshfs -o  default_permissions,IdentityFile=~/.ssh/id_rsa,reconnect admin@asterix:/home/admin /mnt/asterix@admin -p 22341'


######################################################################################

## Find files - tip

## à la https://www.cyberciti.biz/tips/linux-findinglocating-files-with-find-command-part-1.html

######################################################################################



# files modied 3 days ago but less than 5 days.

# find /home -type f -mtime +2 -mtime -5

# find all files with name ‘testfile’ in /home directory recursively and contains the word hello.

# find /home -type f -name testfile | xargs grep -l -i hello 






######################################################################################
# devops remote access
# ssh root access FORBIDDEN --> admin
#
# à la https://www.cyberciti.biz/faq/how-to-set-up-ssh-keys-on-linux-unix/
# à la https://www.cyberciti.biz/tips/ssh-public-key-based-authentication-how-to.html

# xclip -sel clip < ~/.ssh/id_rsa.pub



##########################
#  Mapuche Blog Wordpress ssh access
alias mblog='ssh -t root@173.246.106.175 "cd /var/www/blog && su - www-data ; bash"'
###########################



###
# Asterix ssh admin access
##


# @ Asterix VPS accedemos a puerto no estándar
alias sax='ssh -t admin@asterix  -p 22341'
alias saxroot='ssh -t admin@asterix  -p 22341 "sudo su - ; bash"'

alias console='ssh 185.26.126.77@console.gandi.net'

### Proximacentauri
alias spi='ssh jordi@proximacentauri.librebits.net -p 119'

###
# Sirio ssh jordi access
##
# alias sshso='ssh sirio "source .bash_aliases ; bash"'
alias sso='ssh sirio'


# @ Sirio VPS accedemos a puerto estándar
# alias sshso='ssh sirio "source .bash_aliases ; bash"'  \!/
# alias sshso='ssh sirio "clear ; bash"'  
# alias sshso='ssh sirio'





############################
# Trivial da Terra  ssh a
############################



# @ Asterix VPS accedemos a puerto no estándar
alias sshtr='ssh trivialdaterra@46.231.127.144'



############################
#  M@S server @ Ionos :
#             82.223.115.104
############################

alias sshmas='ssh root@mas'



###########
# gandi-cli
###########
alias gandicli='cd ~/Utils/gandi.cli && virtualenv env && source env/bin/activate'
# alias gandicli='cd ~/Utils/gandi.cli && python3 -m virtualenv env && source env/bin/activate'
# (env) jordi@i-ching:~/Utils/gandi.cli$ python setup.py develop
##########
#
# crear VM
# gandi vm create --datacenter US --memory 512 --ip-version 4 --login jordila --password --hostname temp1 --image "Debian 7 64 bits (HVM)"
# ++RAM
# gandi vm update --memory 1024 patagon01

# via Guarani gandi VPS
# alias clonsite='rsync -ave  ssh 173.246.106.175:/var/www/latinamericajourneys/  /srv/datadisk01/admin/www/latinamericajourneys'
  
# alias clonlatjpanoramix='rsync -ave  ssh 185.26.126.77:/var/html/www/latinamericajourneys/  /var/www/html/latinamericajourneys'

# rsync -ave  ssh 185.26.126.77:/var/www/html/latinamericajourneys/  /var/www/html/latinamericajourneys

# rsync -ave  ssh /var/www/html/latinamericajourneys/  185.26.127.17:/var/www/html/latinamericajourneys


# Drupal8 - Deploy & permissions fix script
##
# drupal permissions fix script
# alias fix-permissions='sudo sh /usr/local/bin/permissions/fix-permissions.sh'

alias fixp='sudo chown -R  :www-data terramondo8.9.0 &&  sudo chmod 775 -R terramondo8.9.0/storage'



# Drupal7 permissions fix script
##
alias fix-permissions='sudo sh /usr/local/bin/permissions/fix-permissions.sh'

alias fix-permissions-latj='sudo sh /usr/local/bin/permissions/fix-permissions.sh --drupal_path=/var/www/html/latinamericajourneys/_www --drupal_user=jordi'

alias fix-permissions-latj-blog='sudo sh /usr/local/bin/permissions/fix-permissions-p.sh --drupal_path=/var/www/html/platform/latinamericajourneys-blog/_www --drupal_user=jordi'

alias  remapspace='xmodmap -e "keycode 166 = space"'
alias poezio='cd ~/Utils/poezio/ && .launch.sh '

alias poezio='cd ~/Utils/poezio/ && .launch.sh '

# config/poezio/poezio.cfg
alias Poezio='cd ~/Utils/poezio'
alias ipub='host myip.opendns.com resolver1.opendns.com'
alias blueoff='bluetooth off'
alias blueon='bluetooth on'

# Open VPN - client TEST
# alias vpn='sudo openvpn /etc/openvpn/client/anarres-AMS2.ovpn'

alias Waterfox='cd ~/Utils/waterfox-classic/ && ./waterfox'


alias ki3lock='sudo killall i3lock'
alias ki3='killall i3'

alias jordi='sudo mount /dev/sdb3 /mnt/120GBdev && cd /mnt/120GBdev/home/jordi'
alias debian='sudo mount /dev/sdb6 /mnt/120GBdeb && cd /mnt/120GBdeb/home'
alias uSD='cd /media/f3n1x/1C9F-A726/'


# PHP
alias phpi='php -i |grep "php.ini"'
alias phpini='php --ini | grep php.ini'


####
## Laravel PHP artisan


alias pharse='php artisan serve'
alias pharcc="php artisan cache:clear"


# inicializar la base de datos
# php artisan migrate && php artisan migrate:fresh


# php artisan migrate && php artisan migrate:fresh

## php artisan tinker

# App\User::count();
# para contar el numero de usarios DDBB	                         

# October CMS

# $ php artisan october:env
#     .env configuration file has been created.
alias phaoenv='php artisan october:env'

#
# php artisan ..
# php artisan october:util set build                              
# php artisan october:util compile js                             
# php artisan october:util compile less                           
# php artisan october:util compile scss                           
# php artisan october:util compile assets                         
# php artisab october:util compile lang                           
# php artisan october:util purge
# php artisan october:util purge uploads
# php october:util purge orphans                         #
# php october:util git pull                               
                           


# Symfony PHP framework 
alias s7ycc='php bin/console cache:pool:clear cache.global_clearer'
alias s7yserve='symfony server:start'
alias s7yserve1='symfony server:start --port=8001 --no-tls'
alias s7yserve2='symfony server:start --port=8002 --no-tls'

# alias tnk='tmux new -s KLEGGICB'


# alias sfydocs='cd ~/PHP/symfony-docs/_build && php -S localhost:8000 -t output'

# alias seamonkey='./home/f3n1x/seamonkey/seamonkey'
alias papertest='paperboy send life-coaching pilot'

alias suspend='loginctl suspend'
